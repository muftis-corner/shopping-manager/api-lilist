'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Items', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      idProduct: {
        type: Sequelize.INTEGER,
        allowNull:false,
      },
      idShop: {
        type: Sequelize.INTEGER,
        allowNull:false,
      },
      idCurrency: {
        type: Sequelize.INTEGER,
        allowNull:false,
      },
      price: {
        type: Sequelize.DOUBLE,
        allowNull:false,
      },
      createdBy: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      isDeleted: {
        type: Sequelize.INTEGER,
        allowNull:false,
        defaultValue:0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
    .then(()=>queryInterface.addConstraint(
      'Items',
      {
        fields: ['idProduct'],
        type: 'foreign key',
        name: 'idProductFK',
        references: {
          table: 'Products',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      })).then(()=> queryInterface.addConstraint(
        'Items',
      {
        fields:['idShop'],
        type: 'foreign key',
        name: 'idShopFK',
        references: {
          table: 'Shops',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      )).then(()=>queryInterface.addConstraint(
        'Items',
      {
        fields:['idCurrency'],
        type: 'foreign key',
        name: 'idCurrencyFK',
        references: {
          table: 'Currencies',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      }
      ));
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Items');
  }
};