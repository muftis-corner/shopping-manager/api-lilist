'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Shops', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      shop:{
        type:Sequelize.STRING,
        alowNull:false
      },
      longitude: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      latitude: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      address: {
        type: Sequelize.STRING
      },
      categories:{
        type: Sequelize.STRING,
        allowNull:false,
        defaultValue: "0",
      },
      createdBy: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      isDeleted: {
        type: Sequelize.INTEGER,
        allowNull:false,
        defaultValue:0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Shops');
  }
};