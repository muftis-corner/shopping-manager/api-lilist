'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Currencies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      currency: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      alias: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      format: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      createdBy: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      isDeleted: {
        type: Sequelize.INTEGER,
        allowNull:false,
        defaultValue:0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Currencies');
  }
};