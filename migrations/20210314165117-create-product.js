'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      barcode: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      productName: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      netValue: {
        type: Sequelize.DOUBLE,
        allowNull:false,
      },
      idImage: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references:{
          model:"Images",
          key:"id"
        }

      },
      idCategory: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references:{
          model:"Categories",
          key:"id"
        }
      },
      idBrand: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references:{
          model:"Brands",
          key:"id"
        }
      },
      idNet: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references:{
          model:"Nets",
          key:"id"
        }
      },
      createdBy: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      isDeleted: {
        type: Sequelize.INTEGER,
        allowNull:false,
        defaultValue:0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
    // .then(()=>queryInterface.addConstraint(
    //   'Products',{
    // fields:['idCategory'],
    //   type: 'foreign key',
    //   name: 'idCategoryFK',
    //   references: {
    //     table: 'Categories',
    //     field: 'id'
    //   },
    //   onDelete: 'cascade',
    //   onUpdate: 'cascade'
    // }
    // )).then(()=>queryInterface.addConstraint(
    //   'Products', {
    //   fields: ['idBrand'],
    //   type: 'foreign key',
    //   name: 'idBrandFK',
    //   references: {
    //     table: 'Brands',
    //     field: 'id'
    //   },
    //   onDelete: 'cascade',
    //   onUpdate: 'cascade'
    // }
    // )).then(()=>queryInterface.addConstraint(
    //   'Products',{
    //     fields:['idNet'],
    //   type: 'foreign key',
    //   name: 'idNetFK',
    //   references: {
    //     table: 'Nets',
    //     field: 'id'
    //   },
    //   onDelete: 'cascade',
    //   onUpdate: 'cascade'
    // }
    // ));
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Products');
  }
};