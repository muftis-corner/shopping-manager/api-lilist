'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  product.init({
    barcode: DataTypes.STRING,
    productName: DataTypes.STRING,
    netValue: DataTypes.INTEGER,
    idImage: DataTypes.INTEGER,
    idCategory: DataTypes.INTEGER,
    idBrand: DataTypes.INTEGER,
    idNet: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    isDeleted: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Product',
  });
  return product;
};