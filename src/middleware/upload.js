const path =  require('path');
const multer = require('multer');

const imageFilter = (req, file, cb) => {
    if (file.mimetype.startsWith("image")) {
      cb(null, true);
    } else {
      cb("Please upload only images.", false);
    }
  };  

const diskStorage = multer.diskStorage({
    destination: (req, file, cb)=>{
        cb(null, path.join(__dirname,"../../uploads"));
    },
    filename: (req, file, cb)=>{
        cb(null, file.fieldname + "-" +Date.now() + path.extname(file.originalname));
    }
});
let uploadImage = multer({storage:diskStorage,fileFilter: imageFilter});
module.exports = uploadImage;