'use strict'
const fs = require('fs');
const path = require('path')
const compress = require("../utils/image-compress");

exports.testUpload = async (req,res)=>{
    
    let imageBuffer =await compress.toBuffer(req.file.path,0.25);

    res.send(`${imageBuffer.byteLength}`);
}