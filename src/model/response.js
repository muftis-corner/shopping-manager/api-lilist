let prod = require('../config/prod');
module.exports = class Response{
    
    static ok = (data,message) => new Response(true,200,`${message}`,data);
    static created = (data,message) => new Response(true,201,`${message}`,data);
    static accepted = (message) => new Response(true,200,`${message}`,[]);
    static noData = (message) => new Response(true,204,`${message}`,[]);

    static error = (error) => {
        if(prod()){
            return new Response(false,500,"Oops, Something wrong",'Internal Server Error');
        }
            console.log(error);
            return new Response(false,500,"Oops, Something wrong",error==null?[]:error);
    };
    static badRequest = (message)=> new Response(false,400,'bad request',[]);
    static unauthorized = (message)=> new Response(false,401,'unauthorized',[]);
    static forbidden = (message)=> new Response(false,403,'forbidden',[]);
    static notFound = (message)=> new Response(false,404,'not found',[]);
    static requestTimedOut = (message)=> new Response(false,408,'Request Timed Out',[]);

    constructor(status,code,message,data){
        this.status = status;
        this.code = code;
        this.message = message;
        this.data = data;
    }
}