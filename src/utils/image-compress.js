'use-strict'

const fs = require('fs');
const sharp = require('sharp');
const sizeOf = require('image-size');


exports.toBuffer = async function (imageSrc,scale) {
  let dimension = sizeOf(imageSrc);
  if(!scale){
    scale = 0.75
  }

  let height = Math.floor(dimension.height*scale);
  let width = Math.floor(dimension.width*scale);

  let imageBuffer =  await sharp(imageSrc).resize(width,height).webp().toBuffer();

  fs.unlinkSync(imageSrc)

  return imageBuffer;
}
exports.toFile = function (imageSrc,output,scale,[callback]) {
  let dimension = sizeOf(imageSrc);
  console.log(dimension);
  if(!scale){
    scale = 0.75
  }

  let height = Math.floor(dimension.height*scale);
  let width = Math.floor(dimension.width*scale);

  return sharp(imageSrc).resize(width,height).toFile(output,(err,info)=>{
    if(err){
      console.log(err)
    } else {
      console.log(info)
      fs.unlinkSync(imageSrc);
      return output
    }
  })
}



// const compressImages = require("compress-image");
// function compress (src,out){
//     return compressImages(
//         src,out,
//         { compress_force: false, statistic: true, autoupdate: true },
//         false,
//         { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
//         { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
//         { svg: { engine: "svgo", command: "--multipass" } },
//         {
//           gif: { engine: "gifsicle", command: ["--colors", "64", "--use-col=web"] },
//         },
//         function (err, completed) {
//           if (completed === true) {
//             console.log(completed);
//           }
//         }
//       );
// }
