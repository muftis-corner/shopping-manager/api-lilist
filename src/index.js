'use strict';

require("dotenv").config();
const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const uploadImage = require('./middleware/upload');

//multer
const multer =require('multer');

const { welcome } = require('./controller/init.controller');
const { createCategory, readCategory ,updateCategory ,deleteCategory, restoreCategory, destroyCategory} = require('./controller/category.controller');
const { createNet, readNet ,deleteNet ,updateNet, restoreNet, destroyNet} = require('./controller/net.controller');
const { createBrand, readBrand ,updateBrand ,deleteBrand, restoreBrand, destroyBrand} = require('./controller/brand.controller');
const { createCurrency, readCurrency, updateCurrency, deleteCurrency, restoreCurrency, destroyCurrency} = require('./controller/currency.controller');
const { createShop, readShop, updateShop, deleteShop, restoreShop, destroyShop} = require('./controller/shop.controller');
const { createProduct, readProduct, updateProduct, deleteProduct, restoreProduct, destroyProduct} = require('./controller/product.controller');
const { createRecord, readRecord, updateRecord, deleteRecord, restoreRecord, destroyRecord} = require('./controller/record.controller');
const { createItem, readItem, updateItem,deleteItem, restoreItem, destroyItem } = require('./controller/item.controller');

//test
const {testUpload}  = require('./test/test');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());

app.get('/', welcome);

app.post('/category/create', createCategory);
app.post('/category/read', readCategory);
app.post('/category/update',updateCategory);
app.post('/category/delete', deleteCategory);
app.post('/category/restore', restoreCategory);
app.post('/category/destroy', destroyCategory);

app.post('/net/create', createNet);
app.post('/net/read', readNet);
app.post('/net/update',updateNet);
app.post('/net/delete', deleteNet);
app.post('/net/restore', restoreNet);
app.post('/net/destroy', destroyNet);

app.post('/brand/create', createBrand);
app.post('/brand/read', readBrand);
app.post('/brand/update',updateBrand);
app.post('/brand/delete', deleteBrand);
app.post('/brand/restore', restoreBrand);
app.post('/brand/destroy', destroyBrand);

app.post('/currency/create', createCurrency);
app.post('/currency/read', readCurrency);
app.post('/currency/update',updateCurrency);
app.post('/currency/delete', deleteCurrency);
app.post('/currency/restore', restoreCurrency);
app.post('/currency/destroy', destroyCurrency);

app.post('/shop/create', createShop);
app.post('/shop/read', readShop);
app.post('/shop/update',updateShop);
app.post('/shop/delete', deleteShop);
app.post('/shop/restore', restoreShop);
app.post('/shop/destroy', destroyShop);

app.post('/product/create',uploadImage.single('image'), createProduct);
app.post('/product/read', readProduct);
app.post('/product/update',updateProduct);
app.post('/product/delete', deleteProduct);
app.post('/product/restore', restoreProduct);
app.post('/product/destroy', destroyProduct);

app.post('/record/create', createRecord);
app.post('/record/read', readRecord);
app.post('/record/update',updateRecord);
app.post('/record/delete', deleteRecord);
app.post('/record/restore', restoreRecord);
app.post('/record/destroy', destroyRecord);

app.post('/item/create', createItem);
app.post('/item/read', readItem);
app.post('/item/update',updateItem);
app.post('/item/delete', deleteItem);
app.post('/item/restore', restoreItem);
app.post('/item/destroy', destroyItem);

app.post('/test',uploadImage.single("photo"),testUpload);

app.listen(process.env.PORT, () => console.log(
    `${process.env.SERVICE_NAME} running on PORT:${process.env.PORT}`
));