'use strict'

const {Op} = require('sequelize');

const db = require('../../models/index');
const Category = db.Category;

const Response = require("../model/response");

exports.readCategory = (req,res)=>{
    var categoryId = parseInt(req.body.id);
    var limit = parseInt(req.body.limit);
    var page = parseInt(req.body.page);
    var query = req.body.query;
    
    if(!query) query = '';
    if(!limit) limit = 10000;
    if(!page) page = 0;
    

    if(categoryId){
        Category.findOne({
            where: {
                id:categoryId
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));;
    } else {
        Category.findAll({
            limit: limit,
            offset: page*limit,
            where: {
                category: {
                    [Op.like]: '%' + query + '%'
                }
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    }
};

exports.createCategory = (req,res)=>{
    Category.create({
        category: req.body.category,
        createdBy: req.body.createdBy,
    })
    .then((result)=>{
        if(result){
            res.send(Response.created(result,'Category Created'));    
        } else {
            res.send(Response.noData('no data'));    
        }
    })
    .catch((err)=>res.send(Response.error(err)));
};

exports.updateCategory = (req,res)=>{
    Category.findOne({
        where: {
            id:req.body.id,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Category.update({
                category: req.body.category,
                updatedBy: req.body.updatedBy,
            },
            {
                where:{
                    id:req.body.id
                }
            })
            .then((result)=>{
                Category.findOne({
                    where: {
                        id:req.body.id,
                        isDeleted:0
                    }
                })
                .then((result)=>res.send(Response.created(result,'Category Updated')))
                .catch((err)=>res.send(Response.error(err)));
            })
            .catch((err)=>res.send(Response.error(err)));
        } else {
            res.send(Response.noData('Category Not Found'))
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
    
};

exports.deleteCategory = (req,res)=>{
    var categoryId = parseInt(req.body.id);
    Category.findOne({
        where: {
            id:categoryId,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Category.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 1
            },{
                where: {
                id:categoryId,
            }})
            .then((result)=>{
                
                    res.send(Response.created(result,'Category Deleted'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Category Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.restoreCategory = (req,res)=>{
    var categoryId = parseInt(req.body.id);
    Category.findOne({
        where: {
            id:categoryId,
            isDeleted:1
        }
    }).then((result)=>{
        var data = result;
        if(result){
            Category.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 0
            },{
                where: {
                id:categoryId,
            }})
            .then((result)=>{
                    res.send(Response.created(data,'Category Restored'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Category Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.destroyCategory = (req, res)=>{
    Category.destroy({
        where:{
            id:req.body.id,
        }
    }).then((result)=>res.send(Response.created(req.body.id,'Category Destroyed')))
    .catch((err)=>{res.send(Response.error(err))});
}
