'use strict'

const {Op} = require('sequelize');

const db = require('../../models/index');
const Record = db.Record;

const Response = require("../model/response");

exports.readRecord = (req,res)=>{
    var recordId = parseInt(req.body.id);
    var limit = parseInt(req.body.limit);
    var page = parseInt(req.body.page);
    var query = req.body.query

    if(!query) query = '';
    if(!limit) limit = 10000;
    if(!page) page = 0;
    
    if(recordId){
        Record.findOne({
            where: {
                id:recordId
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));;
    } else {
        Record.findAll({
            limit: limit,
            offset: page*limit,
            order: [
                ['id', 'DESC'],
            ],
            // where: {
            //     record: {
            //         [Op.like]: '%' + query + '%'
            //     }
            // }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    }
};

exports.createRecord = (req,res)=>{
    Record.create({
        itemRecords: req.body.itemRecords,
        createdBy: req.body.createdBy,
    })
    .then((result)=>{
        if(result){
            res.send(Response.created(result,'Record Created'));    
        } else {
            res.send(Response.noData('no data'));    
        }
    })
    .catch((err)=>res.send(Response.error(err)));
};

exports.updateRecord = (req,res)=>{
    Record.findOne({
        where: {
            id:req.body.id,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Record.update({
                itemRecords: req.body.itemRecords,
                updatedBy: req.body.updatedBy,
            },
            {
                where:{
                    id:req.body.id
                }
            })
            .then((result)=>{
                Record.findOne({
                    where: {
                        id:req.body.id,
                        isDeleted:0
                    }
                })
                .then((result)=>res.send(Response.created(result,'Record Updated')))
                .catch((err)=>res.send(Response.error(err)));
            })
            .catch((err)=>res.send(Response.error(err)));
        } else {
            res.send(Response.noData('Record Not Found'))
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
    
};

exports.deleteRecord = (req,res)=>{
    var recordId = parseInt(req.body.id);
    Record.findOne({
        where: {
            id:recordId,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Record.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 1
            },{
                where: {
                id:recordId,
            }})
            .then((result)=>{
                
                    res.send(Response.created(result,'Record Deleted'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Record Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.restoreRecord = (req,res)=>{
    var recordId = parseInt(req.body.id);
    Record.findOne({
        where: {
            id:recordId,
            isDeleted:1
        }
    }).then((result)=>{
        var data = result;
        if(result){
            Record.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 0
            },{
                where: {
                id:recordId,
            }})
            .then((result)=>{
                    res.send(Response.created(data,'Record Restored'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Record Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});;
};

exports.destroyRecord = (req, res)=>{
    Record.destroy({
        where:{
            id:req.body.id,
        }
    }).then((result)=>res.send(Response.created(req.body.id,'Record Destroyed')))
    .catch((err)=>{res.send(Response.error(err))});
}
