'use strict'

const {Op} = require('sequelize');
const db = require('../../models/index');
const compress = require('../utils/image-compress')
const fs = require('fs');
const Product = db.Product;
const Image = db.Image;



const Response = require("../model/response");

exports.readProduct = (req,res)=>{
    console.log(req.body);
    var productId = parseInt(req.body.id);
    var limit = parseInt(req.body.limit);
    var page = parseInt(req.body.page);
    var query = req.body.query

    if(!query) query = '';
    if(!limit) limit = 10000;
    if(!page) page = 0;
    
    if(productId){
        Product.findOne({
            where: {
                id:productId
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    } else {
        Product.findAll({
            limit: limit,
            offset: page*limit,
            where: {
                productName: {
                    [Op.like]: '%' + query + '%'
                }
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    }
};

exports.createProduct = async (req,res)=>{
    console.log(req.file.filename);
    // compress('/../../uploads/'+req.file.filename,'../../out/');
    Image.create({
        image: fs.readFileSync(__dirname + "/../../out/" + req.file.filename),
        createdBy: req.body.createdBy,
    }).then((result=>{
        
        Product.create({
            barcode:req.body.barcode,
            productName: req.body.productName,
            netValue: req.body.netValue,
            idImage:result.dataValues.id,
            idCategory: req.body.idCategory,
            idBrand:req.body.idBrand,
            idNet:req.body.idNet,
            createdBy: req.body.createdBy,
        })
        .then((result)=>{
            if(result){
                res.send(Response.created(result,'Product Created'));    
            } else {
                res.send(Response.noData('no data'));    
            }
        })
        .catch((err)=>res.send(Response.error(err)));
    })).catch((err)=>res.send(Response.error(err)))    
};

exports.updateProduct = (req,res)=>{
    Product.findOne({
        where: {
            id:req.body.id,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Product.update({
                barcode:req.body.barcode,
                productName: req.body.productName,
                netValue: req.body.netValue,
                idImage: req.body.image,
                idCategory: req.body.idCategory,
                idBrand:req.body.idBrand,
                idNet:req.body.idNet,
                updatedBy: req.body.updatedBy,
            },
            {
                where:{
                    id:req.body.id
                }
            })
            .then((result)=>{
                Product.findOne({
                    where: {
                        id:req.body.id,
                        isDeleted:0
                    }
                })
                .then((result)=>res.send(Response.created(result,'Product Updated')))
                .catch((err)=>res.send(Response.error(err)));
            })
            .catch((err)=>res.send(Response.error(err)));
        } else {
            res.send(Response.noData('Product Not Found'))
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
    
};

exports.deleteProduct = (req,res)=>{
    var productId = parseInt(req.body.id);
    Product.findOne({
        where: {
            id:productId,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Product.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 1
            },{
                where: {
                id:productId,
            }})
            .then((result)=>{
                
                    res.send(Response.created(result,'Product Deleted'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Product Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.restoreProduct = (req,res)=>{
    var productId = parseInt(req.body.id);
    Product.findOne({
        where: {
            id:productId,
            isDeleted:1
        }
    }).then((result)=>{
        var data = result;
        if(result){
            Product.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 0
            },{
                where: {
                id:productId,
            }})
            .then((result)=>{
                    res.send(Response.created(data,'Product Restored'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Product Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});;
};

exports.destroyProduct = (req, res)=>{
    Product.destroy({
        where:{
            id:req.body.id,
        }
    }).then((result)=>res.send(Response.created(req.body.id,'Product Destroyed')))
    .catch((err)=>{res.send(Response.error(err))});
}
