'use strict'

const {Op} = require('sequelize');

const db = require('../../models/index');
const Shop = db.Shop;

const Response = require("../model/response");

exports.readShop = (req,res)=>{
    var shopId = parseInt(req.body.id);
    var limit = parseInt(req.body.limit);
    var page = parseInt(req.body.page);
    var query = req.body.query

    if(!query) query = '';
    if(!limit) limit = 10000;
    if(!page) page = 0;
    
    if(shopId){
        Shop.findOne({
            where: {
                id:shopId
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));;
    } else {
        Shop.findAll({
            limit: limit,
            offset: page*limit,
            where: {
                shop: {
                    [Op.like]: '%' + query + '%'
                }
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    }
};

exports.createShop = (req,res)=>{
    Shop.create({
        shop: req.body.shop,
        createdBy: req.body.createdBy,
        longitude: req.body.longitude,
        latitude: req.body.latitude,
        address: req.body.address,
    })
    .then((result)=>{
        if(result){
            res.send(Response.created(result,'Shop Created'));    
        } else {
            res.send(Response.noData('no data'));    
        }
    })
    .catch((err)=>res.send(Response.error(err)));
};

exports.updateShop = (req,res)=>{
    Shop.findOne({
        where: {
            id:req.body.id,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Shop.update({
                shop: req.body.shop,
                updatedBy: req.body.updatedBy,
                longitude: req.body.longitude,
                latitude: req.body.latitude,
                address: req.body.address,
            },
            {
                where:{
                    id:req.body.id
                }
            })
            .then((result)=>{
                Shop.findOne({
                    where: {
                        id:req.body.id,
                        isDeleted:0
                    }
                })
                .then((result)=>res.send(Response.created(result,'Shop Updated')))
                .catch((err)=>res.send(Response.error(err)));
            })
            .catch((err)=>res.send(Response.error(err)));
        } else {
            res.send(Response.noData('Shop Not Found'))
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
    
};

exports.deleteShop = (req,res)=>{
    var shopId = parseInt(req.body.id);
    Shop.findOne({
        where: {
            id:shopId,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Shop.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 1
            },{
                where: {
                id:shopId,
            }})
            .then((result)=>{
                
                    res.send(Response.created(result,'Shop Deleted'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Shop Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.restoreShop = (req,res)=>{
    var shopId = parseInt(req.body.id);
    Shop.findOne({
        where: {
            id:shopId,
            isDeleted:1
        }
    }).then((result)=>{
        var data = result;
        if(result){
            Shop.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 0
            },{
                where: {
                id:shopId,
            }})
            .then((result)=>{
                    res.send(Response.created(data,'Shop Restored'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Shop Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});;
};

exports.destroyShop = (req, res)=>{
    Shop.destroy({
        where:{
            id:req.body.id,
        }
    }).then((result)=>res.send(Response.created(req.body.id,'Shop Destroyed')))
    .catch((err)=>{res.send(Response.error(err))});
}
