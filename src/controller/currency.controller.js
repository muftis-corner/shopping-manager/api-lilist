'use strict'

const {Op} = require('sequelize');

const db = require('../../models/index');
const Currency = db.Currency;

const Response = require("../model/response");

exports.readCurrency = (req,res)=>{
    var currencyId = parseInt(req.body.id);
    var limit = parseInt(req.body.limit);
    var page = parseInt(req.body.page);
    var query = req.body.query

    if(!query) query = '';
    if(!limit) limit = 10000;
    if(!page) page = 0;
    
    if(currencyId){ 
        Currency.findOne({
            where: {
                id:currencyId
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));;
    } else {
        Currency.findAll({
            limit: limit,
            offset: page*limit,
            where: {
                currency: {
                    [Op.like]: '%' + query + '%'
                }
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    }
};

exports.createCurrency = (req,res)=>{
    Currency.create({
        currency: req.body.currency,
        alias: req.body.alias,
        format: req.body.format,
        createdBy: req.body.createdBy,
    })
    .then((result)=>{
        if(result){
            res.send(Response.created(result,'Currency Created'));    
        } else {
            res.send(Response.noData('no data'));    
        }
    })
    .catch((err)=>res.send(Response.error(err)));
};

exports.updateCurrency = (req,res)=>{
    Currency.findOne({
        where: {
            id:req.body.id,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Currency.update({
                currency: req.body.currency,
                alias: req.body.alias,
                format: req.body.format,
                updatedBy: req.body.updatedBy,
            },
            {
                where:{
                    id:req.body.id
                }
            })
            .then((result)=>{
                Currency.findOne({
                    where: {
                        id:req.body.id,
                        isDeleted:0
                    }
                })
                .then((result)=>res.send(Response.created(result,'Currency Updated')))
                .catch((err)=>res.send(Response.error(err)));
            })
            .catch((err)=>res.send(Response.error(err)));
        } else {
            res.send(Response.noData('Currency Not Found'))
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
    
};

exports.deleteCurrency = (req,res)=>{
    var currencyId = parseInt(req.body.id);
    Currency.findOne({
        where: {
            id:currencyId,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Currency.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 1
            },{
                where: {
                id:currencyId,
            }})
            .then((result)=>{
                
                    res.send(Response.created(result,'Currency Deleted'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Currency Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.restoreCurrency = (req,res)=>{
    var currencyId = parseInt(req.body.id);
    Currency.findOne({
        where: {
            id:currencyId,
            isDeleted:1
        }
    }).then((result)=>{
        var data = result;
        if(result){
            Currency.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 0
            },{
                where: {
                id:currencyId,
            }})
            .then((result)=>{
                    res.send(Response.created(data,'Currency Restored'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Currency Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.destroyCurrency = (req, res)=>{
    Currency.destroy({
        where:{
            id:req.body.id,
        }
    }).then((result)=>res.send(Response.created(req.body.id,'Currency Destroyed')))
    .catch((err)=>{res.send(Response.error(err))});
}
