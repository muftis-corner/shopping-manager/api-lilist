'use strict'

const {Op} = require('sequelize');

const db = require('../../models/index');
const Net = db.Net;

const Response = require("../model/response");

exports.readNet = (req,res)=>{
    var netId = parseInt(req.body.id);
    var limit = parseInt(req.body.limit);
    var page = parseInt(req.body.page);
    var query = req.body.query

    if(!query) query = '';
    if(!limit) limit = 10000;
    if(!page) page = 0;
    
    if(netId){
        Net.findOne({
            where: {
                id:netId
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));;
    } else {
        Net.findAll({
            limit: limit,
            offset: page*limit,
            where: {
                net: {
                    [Op.like]: '%' + query + '%'
                }
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    }
};

exports.createNet = (req,res)=>{
    Net.create({
        net: req.body.net,
        createdBy: req.body.createdBy,
    })
    .then((result)=>{
        if(result){
            res.send(Response.created(result,'Net Created'));    
        } else {
            res.send(Response.noData('no data'));    
        }
    })
    .catch((err)=>res.send(Response.error(err)));
};

exports.updateNet = (req,res)=>{
    Net.findOne({
        where: {
            id:req.body.id,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Net.update({
                net: req.body.net,
                updatedBy: req.body.updatedBy,
            },
            {
                where:{
                    id:req.body.id
                }
            })
            .then((result)=>{
                Net.findOne({
                    where: {
                        id:req.body.id,
                        isDeleted:0
                    }
                })
                .then((result)=>res.send(Response.created(result,'Net Updated')))
                .catch((err)=>res.send(Response.error(err)));
            })
            .catch((err)=>res.send(Response.error(err)));
        } else {
            res.send(Response.noData('Net Not Found'))
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
    
};

exports.deleteNet = (req,res)=>{
    var netId = parseInt(req.body.id);
    Net.findOne({
        where: {
            id:netId,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Net.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 1
            },{
                where: {
                id:netId,
            }})
            .then((result)=>{
                
                    res.send(Response.created(result,'Net Deleted'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Net Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.restoreNet = (req,res)=>{
    var netId = parseInt(req.body.id);
    Net.findOne({
        where: {
            id:netId,
            isDeleted:1
        }
    }).then((result)=>{
        var data = result;
        if(result){
            Net.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 0
            },{
                where: {
                id:netId,
            }})
            .then((result)=>{
                    res.send(Response.created(data,'Net Restored'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Net Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});;
};

exports.destroyNet = (req, res)=>{
    Net.destroy({
        where:{
            id:req.body.id,
        }
    }).then((result)=>res.send(Response.created(req.body.id,'Net Destroyed')))
    .catch((err)=>{res.send(Response.error(err))});
}
