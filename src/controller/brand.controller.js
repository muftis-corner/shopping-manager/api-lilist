'use strict'

const { Op ,  } = require('sequelize');

const db = require('../../models/index');
const Brand = db.Brand;


const Response = require("../model/response");

exports.readBrand = (req,res)=>{
    var brandId = parseInt(req.body.id);
    var category = parseInt(req.body.category);
    var limit = parseInt(req.body.limit);
    var page = parseInt(req.body.page);
    var query = req.body.query;
    
    if(!query) query = '';
    if(!limit) limit = 10000;
    if(!page) page = 0;
    

    if(brandId){
        Brand.findOne({
            where: {
                id:brandId
            }
        })
        .then((result)=>res.send(Response.ok(categoryToList(result),'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    } else {
        Brand.findAll({
            limit: limit,
            offset: page*limit,
            where: {
                brand: {
                    [Op.like]: '%' + query + '%'
                }
            }
        })
        .then((result)=>{
            let brands = result.map(item=>categoryToList(item));
            if(category) {
                let filtered = brands.filter(item=>
                    item.categories.includes(category)
                );
                return res.send(Response.ok(filtered,'Data Found'))
            } else {
                return res.send(Response.ok(brands,'Data Found'))    
            }
        })
        .catch((err)=>res.send(Response.error(err)));
    }
};

exports.createBrand = (req,res)=>{
    Brand.create({
        brand: req.body.brand,
        categories: req.body.categories,
        createdBy: req.body.createdBy,
    })
    .then((result)=>{
        if(result){
            res.send(Response.created(result,'Brand Created'));    
        } else {
            res.send(Response.noData('no data'));    
        }
    })
    .catch((err)=>res.send(Response.error(err)));
};

exports.updateBrand = (req,res)=>{
    Brand.findOne({
        where: {
            id:req.body.id,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Brand.update({
                brand: req.body.brand,
                updatedBy: req.body.updatedBy,
            },
            {
                where:{
                    id:req.body.id
                }
            })
            .then((result)=>{
                Brand.findOne({
                    where: {
                        id:req.body.id,
                        isDeleted:0
                    }
                })
                .then((result)=>res.send(Response.created(result,'Brand Updated')))
                .catch((err)=>res.send(Response.error(err)));
            })
            .catch((err)=>res.send(Response.error(err)));
        } else {
            res.send(Response.noData('Brand Not Found'))
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
    
};

exports.deleteBrand = (req,res)=>{
    var brandId = parseInt(req.body.id);
    Brand.findOne({
        where: {
            id:brandId,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Brand.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 1
            },{
                where: {
                id:brandId,
            }})
            .then((result)=>{
                    res.send(Response.created(result,'Brand Deleted'));    
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Brand Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.restoreBrand = (req,res)=>{
    var brandId = parseInt(req.body.id);
    Brand.findOne({
        where: {
            id:brandId,
            isDeleted:1
        }
    }).then((result)=>{
        var data = result;
        if(result){
            Brand.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 0
            },{
                where: {
                id:brandId,
            }})
            .then((result)=>{
                    res.send(Response.created(data,'Brand Restored'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Brand Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.destroyBrand = (req, res)=>{
    Brand.destroy({
        where:{
            id:req.body.id,
        }
    }).then((result)=>res.send(Response.created(req.body.id,'Brand Destroyed')))
    .catch((err)=>{res.send(Response.error(err))});
}

function categoryToList(item){
    let categories = item.categories.split(',').map(category=> parseInt(category));
        item.categories = categories;
        return item;
}