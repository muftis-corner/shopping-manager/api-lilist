'use strict'

const {Op} = require('sequelize');

const db = require('../../models/index');
const Item = db.Item;

const Response = require("../model/response");

exports.readItem = (req,res)=>{
    var itemId = parseInt(req.body.id);
    var limit = parseInt(req.body.limit);
    var page = parseInt(req.body.page);
    var query = req.body.query

    if(!query) query = '';
    if(!limit) limit = 10000;
    if(!page) page = 0;
    
    if(itemId){
        Item.findOne({
            where: {
                id:itemId
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));;
    } else {
        Item.findAll({
            limit: limit,
            offset: page*limit,
            where: {
                item: {
                    [Op.like]: '%' + query + '%'
                }
            }
        })
        .then((result)=>res.send(Response.ok(result,'Data Found')))
        .catch((err)=>res.send(Response.error(err)));
    }
};

exports.createItem = (req,res)=>{
    Item.create({
        idProduct: req.body.idProduct,
        idShop: req.body.idShop,
        idCurrency: req.body.idCurrency,
        createdBy: req.body.createdBy,
    })
    .then((result)=>{
        if(result){
            res.send(Response.created(result,'Item Created'));    
        } else {
            res.send(Response.noData('no data'));    
        }
    })
    .catch((err)=>res.send(Response.error(err)));
};

exports.updateItem = (req,res)=>{
    Item.findOne({
        where: {
            id:req.body.id,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Item.update({
                idProduct: req.body.idProduct,
                idShop: req.body.idShop,
                idCurrency: req.body.idCurrency,
                createdBy: req.body.createdBy,
                updatedBy: req.body.updatedBy,
            },
            {
                where:{
                    id:req.body.id
                }
            })
            .then((result)=>{
                Item.findOne({
                    where: {
                        id:req.body.id,
                        isDeleted:0
                    }
                })
                .then((result)=>res.send(Response.created(result,'Item Updated')))
                .catch((err)=>res.send(Response.error(err)));
            })
            .catch((err)=>res.send(Response.error(err)));
        } else {
            res.send(Response.noData('Item Not Found'))
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
    
};

exports.deleteItem = (req,res)=>{
    var itemId = parseInt(req.body.id);
    Item.findOne({
        where: {
            id:itemId,
            isDeleted:0
        }
    }).then((result)=>{
        if(result){
            Item.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 1
            },{
                where: {
                id:itemId,
            }})
            .then((result)=>{
                    res.send(Response.created(result,'Item Deleted'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Item Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});
};

exports.restoreItem = (req,res)=>{
    var itemId = parseInt(req.body.id);
    Item.findOne({
        where: {
            id:itemId,
            isDeleted:1
        }
    }).then((result)=>{
        var data = result;
        if(result){
            Item.update({
                updatedBy: req.body.deletedBy,
                isDeleted: 0
            },{
                where: {
                id:itemId,
            }})
            .then((result)=>{
                    res.send(Response.created(data,'Item Restored'));    
                
            })
            .catch((err)=>{
                console.log(err);
                res.send(Response.error(err))
            });
        } else {
            res.send(Response.noData('Item Not Found'));
        }
    })
    .catch((err)=>{res.send(Response.error(err))});;
};

exports.destroyItem = (req, res)=>{
    Item.destroy({
        where:{
            id:req.body.id,
        }
    }).then((result)=>res.send(Response.created(req.body.id,'Item Destroyed')))
    .catch((err)=>{res.send(Response.error(err))});
}
